const Crawler = require('crawler')
const fs = require('fs')
const url = 'http://www.nenu.edu.cn'
const list = []
const result = []
// fs.writeFile('result.json', '', (err) => { if (err) console.log(err) })
let num = 0

const c = new Crawler({
  maxConnections: 10,
  jQuery: true,
  callback (error, res, done) {
    if (error) return
    if (!res || !res.$) return
    const $ = res.$
    const r = {
      title: $('title').text(),
      link: res.request.uri.href
    }
    num++
    process.stdout.write(`[${num}]`)
    result.push(r)
    $('a').each((i, v) => {
      const href = v.attribs.href
      if (href && href.indexOf('/') === 0 &&
        ~href.indexOf('htm') &&
        !~list.indexOf(href)) {
        list.push(href)
        c.queue(url + href)
      }
    })
    done()
  }
})

c.on('drain',function(){
  console.log('end-loading')
  // fs.appendFileSync('result.json',
  // (JSON.stringify(result, null, 2) + ',\n'),
  // err => { if (err) return })
  fs.writeFile('result.json', (JSON.stringify(result, null, 2) + ',\n'), err => {
    if (err) return;console.log('write success')
  })
});

c.queue(url)
